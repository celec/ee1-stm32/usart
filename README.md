# Basic USART

Follow instructions of:
* [OpenCM3 library](https://github.com/libopencm3/libopencm3)
* [Programming tool](https://github.com/trabucayre/ftdi_cpu_prog)
* [Initial repo](https://github.com/jmfriedt/stm32)

*Note:* for the `ftdi_cpu_prog`, you may need root privilege:
``` bash
$ sudo chmod a+s /usr/bin/stm32flash
```
and make sure that modemmanager is **NOT** installed.


You can took QEMU-STM32 from [here](https://github.com/beckus/qemu_stm32):
``` bash
$ sudo apt-get install build-essential zlib1g-dev libglib2.0-dev libpixman-1-dev libtool libfdt-dev
$ git clone https://github.com/beckus/qemu_stm32 && cd qemu_stm32
$ ./configure --enable-debug --disable-werror --target-list="arm-softmmu" --python=/usr/bin/python2.7 && make
```
Binaries at `arm-softmmu`.

## Tree

```
dir
├──lib
│  ├──...
│  └──opencm3
│      ├──...
│      ├──include
│      └──lib
├──ftdi_cpu_prog
│  └──... (stm32flash.sh)
└──code
   └──... (*.c and Makefiles)
```
