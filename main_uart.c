#define usart1
#include "uart.h"

void display_hex(char c){
	signed char v, k;
	for (k = 1; k >= 0; k--){ 
		v = (c >> (4*k)) & 0xf;
		if (v < 10){ my_putchar(v + '0');}
		else { my_putchar(v - 10 + 'A');}
	}
}

void display_dec(char c){
	signed char v, k;
	k = 100; // c < 256
	while (k > 0){
		v = c / k;
		c -= (v * k);
		k /= 10;
		my_putchar(v + '0');
	}
}	

int main(void) {
	int msk = (1 << 11) | (1 << 12) | (1 << 13);
	unsigned char cp = 0;

	clock_setup();
	init_gpio();
	usart_setup();

	while(1){

		delay(0xffff);
		delay(0xffff);
		led_set(msk);

		display_hex(cp);
		my_putchar('\t');
		display_dec(cp);
		my_putchar('\r');
		my_putchar('\n');

		cp = (cp >= 0xff) ? 0 : cp + 1;

		delay(0xffff);
		delay(0xffff);
		led_clr(msk);
	}
	
	return 0;
}
